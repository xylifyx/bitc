package main

import (
	"fmt"
	"os"

	"gitlab.com/xylifyx/bitc/protocol"

	"github.com/urfave/cli"
)

func NewBitcoinPeer() (*protocol.BitcoinPeer, error) {
	return protocol.NewBitcoinPeer(protocol.IPOf(host), port, magic)
}

func version() error {
	peer, err := NewBitcoinPeer()
	if err != nil {
		return err
	}
	err = peer.VersionHandshake()
	if err != nil {
		return err
	}
	fmt.Printf("%+v\n", peer.Version)
	return nil
}

func mempool() error {
	peer, err := NewBitcoinPeer()
	if err != nil {
		return err
	}
	err = peer.VersionHandshake()
	if err != nil {
		return err
	}
	err = peer.Mempool()

	reqresp := protocol.ReqResp{
		Peer: peer,
	}

	if err != nil {
		return err
	}

	peer.Loop(&reqresp)

	return err
}

func getaddr() error {
	peer, err := NewBitcoinPeer()
	if err != nil {
		return err
	}
	err = peer.VersionHandshake()
	if err != nil {
		return err
	}
	err = peer.Getaddr()

	reqresp := protocol.ReqResp{
		Peer: peer,
	}

	if err != nil {
		return err
	}

	peer.Loop(&reqresp)

	return err
}

func getdata(objectType protocol.ObjectType, args []string) error {
	peer, err := NewBitcoinPeer()
	if err != nil {
		return err
	}
	err = peer.VersionHandshake()
	if err != nil {
		return err
	}
	err = peer.Getdata(objectType, args)

	reqresp := protocol.ReqResp{
		Peer: peer,
	}

	if err != nil {
		return err
	}

	peer.Loop(&reqresp)

	return err
}
func getheaders(args []string) error {
	peer, err := NewBitcoinPeer()
	if err != nil {
		return err
	}
	err = peer.VersionHandshake()
	if err != nil {
		return err
	}
	err = peer.Getheaders(args)

	reqresp := protocol.ReqResp{
		Peer: peer,
	}

	if err != nil {
		return err
	}

	peer.Loop(&reqresp)

	return err
}

func disasm(args []string) {

}


var host string
var port uint16
var magic uint32

func main() {
	host = "127.0.0.1"
	port = 18333
	magic = protocol.Testnet3Magic

	app := cli.NewApp()
	app.Name = "bitc"
	app.Usage = "Explore bitcoin protocol"
	app.Commands = []cli.Command{
		{
			Name:  "mempool",
			Usage: "Sends a request to a node asking for information about transactions it has verified but which have not yet confirmed",
			Action: func(c *cli.Context) error {
				return mempool()
			},
		},
		{
			Name:  "getheaders",
			Usage: "Sends a request to a node asking for information about headers",
			Action: func(c *cli.Context) error {
				return getheaders(c.Args())
			},
		},
		{
			Name:  "getdata",
			Usage: "used in response to inv, to retrieve the content of a specific object, and is usually sent after receiving an inv packet,",
			Action: func(c *cli.Context) error {
				switch c.Args()[0] {
				case "tx":
					return getdata(protocol.MSG_TX, c.Args()[1:])
				case "block":
					return getdata(protocol.MSG_BLOCK, c.Args()[1:])
				case "cmpct":
					return getdata(protocol.MSG_CMPCT_BLOCK, c.Args()[1:])
				case "filtered":
					return getdata(protocol.MSG_FILTERED_BLOCK, c.Args()[1:])
				default:
					return getdata(protocol.MSG_TX, c.Args())
				}
			},
		},
		{
			Name:  "version",
			Usage: "Show version",
			Action: func(c *cli.Context) error {
				return version()
			},
		},
		{
			Name: "getaddr",
			Usage: "Get addresses of nodes",
			Action: func(c *cli.Context) error {
				return getaddr()
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		fmt.Printf("bitc error: %s", err)
	}
}
