package protocol

import (
	"reflect"
	"testing"
)

func Test_netAddr_unmarshall(t *testing.T) {
	hexData := "01 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 FF FF 0A 00 00 01 20 8D"
	marshalled := dehex(hexData)
	unmarshalled := NetAddr{services: NodeNetwork, ip: IPOf("10.0.0.1"), port: 8333}
	{
		var got NetAddr
		err := Unmarshall(func(bw *BitcoinReader) {
			bw.NetAddr(&got, false)
		}, marshalled)

		if err != nil || !reflect.DeepEqual(got, unmarshalled) {
			t.Errorf("%q. NetAddr = \n%v, want \n%v err %v", "NetAddr unmarshalled", got, unmarshalled, err)
		}
	}

	{
		got, err := Marshall(func(bw *BitcoinWriter) {
			bw.NetAddr(&unmarshalled, false)
		})

		if !reflect.DeepEqual(got, marshalled) {
			t.Errorf("%q. []byte = \n%v, want \n%v err %v", "NetAddr marshalled", got, marshalled, err)
		}
	}
}
