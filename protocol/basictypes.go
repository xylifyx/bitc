package protocol

import (
	"encoding/binary"
	"errors"
	"net"
)

// _VarStr serialization format of a VarStr, a VarStr is represented by a normal string
type _VarStr struct {
	length  uint64
	content []byte
}

// Bool1 boolean [1]byte = false:0 true:1 reader
func (coder *BitcoinReader) Bool1(f *bool) {
	buf := [1]byte{0}
	coder.Read(buf[:])
	*f = buf[0] != 0
}

// Bool1 boolean [1]byte = false:0 true:1 reader
func (coder *BitcoinWriter) Bool1(f *bool) {
	var buf [1]byte
	if *f {
		buf[0] = 1
	} else {
		buf[0] = 0
	}
	coder.Write(buf[:])
}

// UI16no uint16 reader
func (coder *BitcoinReader) UI16no(f *uint16) {
	buf := [2]byte{}
	coder.Read(buf[0:2])
	*f = binary.BigEndian.Uint16(buf[0:2])
}

// UI16no uint16 writer
func (coder *BitcoinWriter) UI16no(f *uint16) {
	buf := [2]byte{}
	binary.BigEndian.PutUint16(buf[:], *f)
	coder.Write(buf[:])
}

// VarStr unmarshalling
func (coder *BitcoinReader) VarStr(f *string) {
	c := _VarStr{}
	c.bincode(coder)
	*f = string(c.content)
}

// VarStr marshalling
func (coder *BitcoinWriter) VarStr(f *string) {
	content := []byte(*f)
	c := _VarStr{
		length:  uint64(len(content)),
		content: content,
	}
	c.bincode(coder)
}

func (f *_VarStr) bincode(coder BitcoinBincoder) {
	coder.VarInt(&f.length)
	coder.Bytes(
		int(f.length),
		func() []byte { return f.content },
		func(bs []byte) { f.content = bs },
	)
}

// IP writer
func (coder *BitcoinReader) netIP(f *net.IP) {
	bincodeIP(coder, f)
}

// IP writer
func (coder *BitcoinWriter) netIP(f *net.IP) {
	bincodeIP(coder, f)
}

func bincodeIP(coder BitcoinBincoder, f *net.IP) {
	coder.Bytes(
		16,
		func() []byte {
			ip := *f
			if len(ip) == 0 {
				ip = net.IPv4(0, 0, 0, 0)
			} else if len(ip) == 4 {
				ip = net.IPv4(ip[0], ip[1], ip[2], ip[3])
			} else if len(ip) != 16 {
				coder.SetError(errors.New("Length of IP != 16"))
			}
			return ip[:]
		},
		func(value []byte) { *f = net.IP(value) },
	)
}

// IPOf parses the addr string and returns a single net.IP
func IPOf(addr string) net.IP {
	ips, _ := net.LookupIP(addr)
	return ips[0]
}
