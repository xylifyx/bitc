package protocol

import (
	"io"
	"fmt"
)

func NewGetheaders(ids []string) *Getheaders {
	invs := make([]Hash256, len(ids))
	for idx, id := range ids {
		invs[idx] = NewHash256(id)
	}
	return &Getheaders{
		hashes: invs,
	}
}

/*
Field Size	Description	Data type	Comments
4	version	uint32_t	the protocol version
1+	hash count	var_int	number of block locator hash entries
32+	block locator hashes	char[32]	block locator object; newest back to genesis block (dense to start, but then sparse)
32	hash_stop	char[32]	hash of the last desired block header; set to zero to get as many blocks as possible (2000)
 */

type Getheaders struct {
	hashes    []Hash256
	hash_stop Hash256
}

// Mempool wire format
func (f *Getheaders) encode(w BitcoinBincoder) {
	version := uint32(ProtocolVersion)
	w.UI32(&version)
	count := uint64(len(f.hashes))
	w.VarInt(&count)
	w.Slice(int(count), func(size int) {
		f.hashes = make([]Hash256, size)
	}, func(idx int) {
		w.Hash256(&f.hashes[idx])
	})
	w.Hash256(&f.hash_stop)
}

// Command code
func (f *Getheaders) Command() string {
	return getheadersCmd
}

// Visit for Verack
func (f *Getheaders) Visit(v MessageVisitor) {
	v.Getheaders(f)
}

// GetBlocks unmarshalling
func (coder *BitcoinReader) Getheaders(f *Getheaders) {
	f.encode(coder)
}

// GetBlocks marshalling
func (coder *BitcoinWriter) Getheaders(f *Getheaders) {
	f.encode(coder)
}

// Visual representation
func (f *Getheaders) Visual(w io.Writer) {
	fmt.Fprintf(w, "%s:\n", f.Command())
	for _, elm := range f.hashes {
		fmt.Fprintf(w, "\t%s\n", elm.GetId())
	}
	fmt.Fprintf(w, "\thash_stop: %s\n", f.hash_stop.GetId())
}
