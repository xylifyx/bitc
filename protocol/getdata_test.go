package protocol

import (
	"reflect"
	"testing"
)

func TestInv_Command(t *testing.T) {
	want := *NewGetdata(2, []string{"36bd5424e426abda5a772acc36c1bb38a21a020e67aa483f947cfc36e2ed3107"})
	inv := *NewInv(2, []string{"36bd5424e426abda5a772acc36c1bb38a21a020e67aa483f947cfc36e2ed3107"})
	var got GetData
	marshalled, _ := Marshall(func(br *BitcoinWriter) {
		br.Getdata(&want)
	})

	err := Unmarshall(func(br *BitcoinReader) {
		br.Getdata(&got)
	}, marshalled)
	marshalled2, _ := Marshall(func(br *BitcoinWriter) {
		br.Getdata(&got)
	})
	marshalled3, _ := Marshall(func(br *BitcoinWriter) {
		br.Inv(&inv)
	})
	if err != nil {
		t.Errorf("err %v", err)
	}
	if !reflect.DeepEqual(got, want) {
		t.Errorf("got %+v want %+v", got, want)
	}
	if !reflect.DeepEqual(marshalled, marshalled2) {
		t.Errorf("got %v want %v", marshalled, marshalled2)
	}
	if !reflect.DeepEqual(marshalled, marshalled3) {
		t.Errorf("got %v want %v", marshalled, marshalled2)
	}
}
