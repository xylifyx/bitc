package protocol

import (
	"crypto/sha256"
	"io"

	"golang.org/x/crypto/ripemd160"
)

type Hash256 [32]byte
type Hash160 [20]byte

func (f *Hash256) Visual(w io.Writer) {
	w.Write([]byte(f.GetId()))
}

func dhash(c []byte) []byte {
	sum1 := sha256.Sum256(c)
	sum2 := sha256.Sum256(sum1[:])
	return sum2[:]
}

func dhash256(c []byte) Hash256 {
	sum1 := sha256.Sum256(c)
	sum2 := sha256.Sum256(sum1[:])
	return sum2
}

func rdhash(c []byte) []byte {
	sum1 := sha256.Sum256(c)
	ripdig := ripemd160.New()
	ripdig.Write(sum1[:])
	hash := []byte{}
	return ripdig.Sum(hash)
}

func reverse(s []byte) {
	for i, j := 0, len(s)-1; i < j; i, j = i+1, j-1 {
		s[i], s[j] = s[j], s[i]
	}
}

func merkleHash(c [][]byte) []byte {
	if len(c) > 2 {
		m := (len(c) + 1) / 2
		concat := append(merkleHash(c[0:m]), merkleHash(c[m:])...)
		return dhash(concat)
	} else if len(c) == 2 {
		concat := append(dhash(c[0]), dhash(c[1])...)
		return dhash(concat)
	} else if len(c) == 1 {
		concat := append(dhash(c[0]), dhash(c[0])...)
		return dhash(concat)
	}
	return []byte{}
}
