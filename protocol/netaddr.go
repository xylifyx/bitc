package protocol

import (
	"fmt"
	"io"
	"net"
)

// NetAddr When a network address is needed somewhere, this structure is used. Network addresses are not prefixed with a timestamp in the version message.
type NetAddr struct {
	// Field Size	Description	Data type	Comments
	// 4	time	uint32	the Time (version >= 31402). Not present in version message.
	time uint32
	// 8	services	uint64_t	same service(s) listed in version
	services uint64
	// 16	IPv6/4	char[16]	IPv6 address. Network byte order.
	ip net.IP
	// The original client only supported IPv4 and only read the last 4 bytes to get the IPv4 address.
	// However, the IPv4 address is written into the message as a 16 byte IPv4-mapped IPv6 address
	// (12 bytes 00 00 00 00 00 00 00 00 00 00 FF FF, followed by the 4 bytes of the IPv4 address).

	//2	port	uint16_t	port number, network byte order
	port uint16
}

// NetAddr unmarshaller
func (coder *BitcoinReader) NetAddr(f *NetAddr, timePresent bool) {
	f.encode(coder, timePresent)
}

// NetAddr marshaller
func (coder *BitcoinWriter) NetAddr(f *NetAddr, timePresent bool) {
	f.encode(coder, timePresent)
}

func (f *NetAddr) encode(w BitcoinBincoder, timePresent bool) {
	if timePresent {
		w.UI32(&f.time)
	}
	w.UI64(&f.services)
	w.netIP(&f.ip)
	w.UI16no(&f.port)
}

// Visual representation
func (f *NetAddr) Visual(w io.Writer) {
	fmt.Fprintf(w, "%s:%d", f.ip.String(), f.port)
}
