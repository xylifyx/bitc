package protocol

import (
	"fmt"
	"io"
)

// Feefilter is the reqeust for unconfirmed transactions
type Feefilter struct {
	Feerate uint64
}

// Feefilter wire format
func (f *Feefilter) encode(w BitcoinBincoder) {
	w.UI64(&f.Feerate)
}

// Feefilter unmarshalling
func (coder *BitcoinReader) Feefilter(f *Feefilter) {
	f.encode(coder)
}

// Feefilter marshalling
func (coder *BitcoinWriter) Feefilter(f *Feefilter) {
	f.encode(coder)
}

// Visit for Feefilter
func (f *Feefilter) Visit(v MessageVisitor) {
	v.Feefilter(f)
}

// Command name of Feefilter
func (f *Feefilter) Command() string {
	return FeefilterCmd
}

// Visual representation
func (f *Feefilter) Visual(w io.Writer) {
	fmt.Fprintf(w, "%s: %d\n", f.Command(), f.Feerate)
}
