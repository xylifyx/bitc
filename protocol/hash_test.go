package protocol

import (
	"encoding/hex"
	"reflect"
	"regexp"
	"strings"
	"testing"
)

/*

hello
2cf24dba5fb0a30e26e83b2ac5b9e29e1b161e5c1fa7425e73043362938b9824 (first round of sha-256)
9595c9df90075148eb06860365df33584b75bff782a510c6cd4883a419833d50 (second round of sha-256)


For bitcoin addresses (RIPEMD-160) this would give:

hello
2cf24dba5fb0a30e26e83b2ac5b9e29e1b161e5c1fa7425e73043362938b9824 (first round is sha-256)
b6a9c8c230722b7c748331a8b450f05566dc7d0f (with ripemd-160)
*/

func dehex(s string) []byte {
	re, _ := regexp.Compile("-.*\n")
	s = re.ReplaceAllString(s, "")
	s = strings.Replace(s, " ", "", -1)
	s = strings.Replace(s, "\n", "", -1)

	dh, _ := hex.DecodeString(s)
	return dh
}

func Test_dhash(t *testing.T) {
	type args struct {
		c []byte
	}
	tests := []struct {
		name string
		args args
		want []byte
	}{
		{"hello", args{[]byte("hello")}, dehex("9595c9df90075148eb06860365df33584b75bff782a510c6cd4883a419833d50")},
	}
	for _, tt := range tests {
		if got := dhash(tt.args.c); !reflect.DeepEqual(got, tt.want) {
			t.Errorf("%q. dhash() = %v, want %v", tt.name, got, tt.want)
		}
	}
}

func Test_rdhash(t *testing.T) {
	type args struct {
		c []byte
	}
	tests := []struct {
		name string
		args args
		want []byte
	}{
		{"hello", args{[]byte("hello")}, dehex("b6a9c8c230722b7c748331a8b450f05566dc7d0f")},
	}
	for _, tt := range tests {
		if got := rdhash(tt.args.c); !reflect.DeepEqual(got, tt.want) {
			t.Errorf("%q. rdhash() = %v, want %v", tt.name, got, tt.want)
		}
	}
}

func Test_merkleHash(t *testing.T) {
	a := []byte("Foo")
	b := []byte("Bar")
	c := []byte("Baz")

	d1 := dhash(a)
	d2 := dhash(b)
	d3 := dhash(c)
	d4 := dhash(c) // a, b, c are 3. that's an odd number, so we take the c twice

	d5 := dhash(append(d1, d2...))
	d6 := dhash(append(d3, d4...))

	d7 := dhash(append(d5, d6...))

	type args struct {
		c [][]byte
	}
	tests := []struct {
		name string
		args args
		want []byte
	}{
		{"Merkle", args{[][]byte{a, b, c}}, d7},
	}
	for _, tt := range tests {
		if got := merkleHash(tt.args.c); !reflect.DeepEqual(got, tt.want) {
			t.Errorf("%q. merkleHash() = %v, want %v", tt.name, got, tt.want)
		}
	}
}
