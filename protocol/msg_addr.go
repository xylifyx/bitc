package protocol

import (
	"fmt"
	"io"
)

// Addr is the reqeust for unconfirmed transactions
type Addr struct {
	addrList []NetAddr
}

// Addr wire format
func (f *Addr) encode(w BitcoinBincoder) {
	var count uint64
	w.VarInt(&count)
	w.Slice(int(count), func(size int) {
		f.addrList = make([]NetAddr, int(count))
	}, func(idx int) {
		w.NetAddr(&f.addrList[idx], true)
	})
}

// Addr unmarshalling
func (coder *BitcoinReader) Addr(f *Addr) {
	f.encode(coder)
}

// Addr marshalling
func (coder *BitcoinWriter) Addr(f *Addr) {
	f.encode(coder)
}

// Visit for Addr
func (f *Addr) Visit(v MessageVisitor) {
	v.Addr(f)
}

// Command name of Addr
func (f *Addr) Command() string {
	return AddrCmd
}

// Visual representation
func (f *Addr) Visual(w io.Writer) {
	fmt.Fprintf(w, "%s:\n", f.Command())
	for _, elm := range f.addrList {
		w.Write([]byte("- "))
		elm.Visual(w)
		w.Write([]byte("\n"))
	}
}
