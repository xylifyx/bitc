package protocol

import (
	"io"
	"fmt"
)

// Block Headers
// Block headers are sent in a headers packet in response to a getheaders message.



type Headers struct {
	headers []BlockHeader
}

/* Field Size	Description	Data type	Comments
4	version	int32_t	Block version information (note, this is signed)
32	prev_block	char[32]	The hash value of the previous block this particular block references
32	merkle_root	char[32]	The reference to a Merkle tree collection which is a hash of all transactions related to this block
4	timestamp	uint32_t	A timestamp recording when this block was created (Will overflow in 2106[2])
4	bits	uint32_t	The calculated difficulty target being used for this block
4	nonce	uint32_t	The nonce used to generate this block… to allow variations of the header and compute different hashes
1	txn_count	var_int	Number of transaction entries, this value is always 0
*/

type BlockHeader struct {
	version    int32
	prevBlock  Hash256
	merkleRoot Hash256
	timestamp  uint32
	bits       uint32
	nonce      uint32
	txnCount   uint64
}

// Mempool wire format
func (f *Headers) encode(w BitcoinBincoder) {
	count := uint64(len(f.headers))
	w.VarInt(&count)
	w.Slice(int(count), func(size int) {
		f.headers = make([]BlockHeader, size)
	}, func(idx int) {
		w.BlockHeader(&f.headers[idx])
	})
}

// Mempool wire format
func (f *BlockHeader) encode(w BitcoinBincoder) {
	w.I32(&f.version)
	w.Hash256(&f.prevBlock)
	w.Hash256(&f.merkleRoot)
	w.UI32(&f.timestamp)
	w.UI32(&f.bits)
	w.UI32(&f.nonce)
	w.VarInt(&f.txnCount)
}

// BlockHeader unmarshalling
func (coder *BitcoinReader) BlockHeader(f *BlockHeader) {
	f.encode(coder)
}

// BlockHeader marshalling
func (coder *BitcoinWriter) BlockHeader(f *BlockHeader) {
	f.encode(coder)
}

func (f *BlockHeader) Visual (w io.Writer) {
	fmt.Fprintf(w, "\t\tversion: %d\n", f.version)
	fmt.Fprintf(w, "\t\tprevBlock: %s\n", f.prevBlock.GetId())
	fmt.Fprintf(w, "\t\tmerkleRoot: %s\n", f.merkleRoot.GetId())
	fmt.Fprintf(w, "\t\ttimestamp: %d\n", f.timestamp)
	fmt.Fprintf(w, "\t\tbits: %d\n", f.bits)
	fmt.Fprintf(w, "\t\tnonce: %d\n", f.nonce)
	fmt.Fprintf(w, "\t\ttxnCount: %d\n", f.txnCount)
}

func (f *Headers) Visual (w io.Writer) {
	fmt.Fprintf(w, "%s: #%d\n", f.Command(), len(f.headers))
	for idx, elm := range f.headers {
		w.Write([]byte("- "))
		elm.Visual(w)
		if idx > 5 {
			w.Write([]byte("...\n"))
			break
		}
	}
}

// Headers unmarshalling
func (coder *BitcoinReader) Headers(f *Headers) {
	f.encode(coder)
}

// Headers marshalling
func (coder *BitcoinWriter) Headers(f *Headers) {
	f.encode(coder)
}

// Visit for Mempool
func (f *Headers) Visit(v MessageVisitor) {
	v.Headers(f)
}

// Command name of Mempool
func (f *Headers) Command() string {
	return HeadersCmd
}

// Hash256 unmarshalling
func (coder *BitcoinReader) Hash256(f *Hash256) {
	coder.Read(f[:])
}

// Hash256 marshalling
func (coder *BitcoinWriter) Hash256(f *Hash256) {
	coder.Write(f[:])
}
