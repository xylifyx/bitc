package protocol

// Sendheaders is the reqeust for unconfirmed transactions
type Sendheaders struct {
}

// Sendheaders wire format
func (f *Sendheaders) encode(w BitcoinBincoder) {
}

// Sendheaders unmarshalling
func (coder *BitcoinReader) Sendheaders(f *Sendheaders) {
	f.encode(coder)
}

// Sendheaders marshalling
func (coder *BitcoinWriter) Sendheaders(f *Sendheaders) {
	f.encode(coder)
}

// Visit for Sendheaders
func (f *Sendheaders) Visit(v MessageVisitor) {
	v.Sendheaders(f)
}

// Command name of Sendheaders
func (f *Sendheaders) Command() string {
	return sendheadersCmd
}
