package protocol

import "testing"

func TestInterpreter(t *testing.T) {
	//script := dehex("76a9146cf6113139dc7d4e9ddece9cecb067c45e30558d88ac")

}

/*
tx: version: 1
    txin:
    - 885f28b6e3a2bee9e47870333c4d3d20c648ba9d42f047f9a428dff4b07fea2a[490]
    txout:
    - 53710 76a9140beccdd845b5def61165e0bb972e5ff56187f29088ac
    - 49999846289 76a9146cf6113139dc7d4e9ddece9cecb067c45e30558d88ac
	receive: ping
	send: pong
	receive: ping
	send: pong
	receive: ping

BITCOIN TESTNET TRANSACTION
c2f01132242eee488db12ac47f9cb8a81320e84a2f1ac1ea95f88b3d7ad96d29
Estimated TX Value	0.00053710 TBTC
Confirmations	1 CONFIRMATION
Block
Relay time
Tuesday, September 12th 2017, 20:36:41 +02:00
Time until confirmed	after
Total Inputs	500.00000000 tBTC
Total Outputs	499.99899999 tBTC
Fee	0.00100001 tBTC
Fee / KB	0.00442482 tBTC
Size	226 bytes
Estimated Change	499.99846289 tBTC
sent to mqT63pNS3euwYdfnmornGw3Tdzt1G1qWBn
1 INPUTS
Total Inputs: 500.00000000 tBTC
n4qGhY3Jf4dneNtWvY2eEdsqUVX9CQhfQZ(500.00000000)
2 OUTPUTS
499.99846289 tBTC change
mgc1QbmEZUT5wTbwM7xtmgJM7SgoedWqAN(0.00053710)
mqT63pNS3euwYdfnmornGw3Tdzt1G1qWBn(499.99846289)
INPUT SCRIPTS
OUTPUT SCRIPTS
3045022100c848d0ec4ce0e0ada66f36a5f059ac1fbf770c71113f2d46fa17a54e89f075fd0220605ebe2a666ecf899fb45619ea3071c392968922d040e514d471cb152cc4448b01 030f2682a027963cbcfc99acf4af6a80e081409e5fdc9e36364c7f8a7d5534646f
OP_DUP OP_HASH160 0beccdd845b5def61165e0bb972e5ff56187f290 OP_EQUALVERIFY OP_CHECKSIG
 */