package protocol;

type ScriptError struct {
}

type valtype interface {
}

type valstack []valtype
type script struct {
	script []byte
	pc     int
	stack  valstack
}
type opcode byte

func (s *script) opcode() opcode {
	op := s.script[s.pc]
	s.pc++
	return opcode(op)
}

func (s *script) pushdata(n int) {
	data := s.script[s.pc:s.pc+n]
	s.stack = s.stack.push(data)
}

func (s valstack) push(val valtype) valstack {
	if len(s) == cap(s) {
		return append(s, val)
	} else {
		s[len(s)] = val
		return s
	}
}

func (s *valstack) pop_back() {

}

type runner func(s *script)

type inst struct {
	op opcode
	label string
}

/*
const (
	OP_TOALTSTACK   = 0x6b //	x1	(alt)x1	Puts the input onto the top of the alt stack. Removes it from the main stack.
	OP_FROMALTSTACK = 0x6c //	(alt)x1	x1	Puts the input onto the top of the main stack. Removes it from the alt stack.
	OP_IFDUP        = 0x73 //	x	x / x x	If the top stack value is not 0, duplicate it.
	OP_DEPTH        = 0x74 //	Nothing	<Stack size>	Puts the number of stack items onto the stack.
	OP_DROP         = 0x75 //	x	Nothing	Removes the top stack item.
	OP_DUP          = 0x76 //	x	x x	Duplicates the top stack item.
	OP_NIP          = 0x77 //	x1 x2	x2	Removes the second-to-top stack item.
	OP_OVER         = 0x78 //	x1 x2	x1 x2 x1	Copies the second-to-top stack item to the top.
	OP_PICK         = 0x79 //	xn ... x2 x1 x0 <n>	xn ... x2 x1 x0 xn	The item n back in the stack is copied to the top.
	OP_ROLL         = 0x7a //	xn ... x2 x1 x0 <n>	... x2 x1 x0 xn	The item n back in the stack is moved to the top.
	OP_ROT          = 0x7b //	x1 x2 x3	x2 x3 x1	The top three items on the stack are rotated to the left.
	OP_SWAP         = 0x7c //	x1 x2	x2 x1	The top two items on the stack are swapped.
	OP_TUCK         = 0x7d //	x1 x2	x2 x1 x2	The item at the top of the stack is copied and inserted before the second-to-top item.
	OP_2DROP        = 0x6d //	x1 x2	Nothing	Removes the top two stack items.
	OP_2DUP         = 0x6e //	x1 x2	x1 x2 x1 x2	Duplicates the top two stack items.
	OP_3DUP         = 0x6f //	x1 x2 x3	x1 x2 x3 x1 x2 x3	Duplicates the top three stack items.
	OP_2OVER        = 0x70 //	x1 x2 x3 x4	x1 x2 x3 x4 x1 x2	Copies the pair of items two spaces back in the stack to the front.
	OP_2ROT         = 0x71 //	x1 x2 x3 x4 x5 x6	x3 x4 x5 x6 x1 x2	The fifth and sixth items back are moved to the top of the stack.
	OP_2SWAP        = 0x72 //	x1 x2 x3 x4	x3 x4 x1 x2	Swaps the top two pairs of items.
)
*/