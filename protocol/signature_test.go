package protocol

import (
	"testing"
	"encoding/hex"
	"bytes"
)

func TestBase58Enc(t *testing.T) {
	expected := "mv4Pjq3ibssKMMRb8zEtFpEQGoEqjvQEA4"
	got := Base58Enc(TestnetPubkey, dehex("9f83e54cd6454986daeb6a59b0c460f001940270"))
	if got != expected {
		t.Errorf("\nGot %s\nExp %s\n", got, expected)
		t.Errorf("\nGot %s\nExp %s\n", hex.EncodeToString([]byte(got)), hex.EncodeToString([]byte(expected)))
	}

}

func TestBase58Dec(t *testing.T) {
	input := "mv4Pjq3ibssKMMRb8zEtFpEQGoEqjvQEA4"
	expected,_ := hex.DecodeString("9f83e54cd6454986daeb6a59b0c460f001940270")

	got,version := Base58Dec(input)
	if !bytes.Equal(got, expected) {
		t.Errorf("\nGot %s\nExp %s\n", hex.EncodeToString([]byte(got)), hex.EncodeToString([]byte(expected)))
	}
	if version != TestnetPubkey {
		t.Errorf("\nGot %d\nExp %d\n",version,TestnetPubkey)
	}
}

func TestPrivKey(t *testing.T) {
	privkey := "cPaqGNyjAJPVnJb4XbrVGfGF65spyq1unQHHSPRtNSFbFMBCF7my"
	input := privkey
	expected,_ := hex.DecodeString("3bbe0b1206e739105c7b31e1262b88647c060b61db59b7eedb90839c2d504b6401")

	got,version := Base58Dec(input)
	if !bytes.Equal(got, expected) {
		t.Errorf("\nGot %s\nExp %s\n", hex.EncodeToString([]byte(got)), hex.EncodeToString([]byte(expected)))
	}
	if version != TestnetPrivateKey {
		t.Errorf("\nGot %d\nExp %d\n",version,TestnetPubkey)
	}
}


// cPaqGNyjAJPVnJb4XbrVGfGF65spyq1unQHHSPRtNSFbFMBCF7my moccHMqj4vCfR35XY9MkgjSFSVqwM2yjB9