package protocol

// Mempool is the reqeust for unconfirmed transactions
type pingpong struct {
	nounce uint64
}

type Ping struct {
	pingpong
}

type Pong struct {
	pingpong
}

// Mempool wire format
func (f *pingpong) encode(w BitcoinBincoder) {
	w.UI64(&f.nounce)
}

// Ping unmarshalling
func (coder *BitcoinReader) Ping(f *Ping) {
	f.encode(coder)
}

// Ping marshalling
func (coder *BitcoinWriter) Ping(f *Ping) {
	f.encode(coder)
}

// Pong unmarshalling
func (coder *BitcoinReader) Pong(f *Pong) {
	f.encode(coder)
}

// Pong marshalling
func (coder *BitcoinWriter) Pong(f *Pong) {
	f.encode(coder)
}

// Visit for Ping
func (f *Ping) Visit(v MessageVisitor) {
	v.Ping(f)
}

// Visit for Pong
func (f *Pong) Visit(v MessageVisitor) {
	v.Pong(f)
}

// Command name of Ping
func (f *Ping) Command() string {
	return PingCmd
}

// Command name of Pong
func (f *Pong) Command() string {
	return PongCmd
}
