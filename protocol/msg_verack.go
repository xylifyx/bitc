package protocol

// Verack is the response to a Version message
type Verack struct {
}

// Verack wire format
func (f *Verack) encode(w BitcoinBincoder) {
}

// Verack unmarshalling
func (coder *BitcoinReader) Verack(f *Verack) {
	f.encode(coder)
}

// Verack marshalling
func (coder *BitcoinWriter) Verack(f *Verack) {
	f.encode(coder)
}

// Visit for Verack
func (f *Verack) Visit(v MessageVisitor) {
	v.Verack(f)
}

// Command name of Verack
func (f *Verack) Command() string {
	return VerackCmd
}
