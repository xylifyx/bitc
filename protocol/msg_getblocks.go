package protocol

import (
	"fmt"
	"io"
)

// Getblocks retrieves objects
type Getblocks struct {
	// 4	version	uint32_t	the protocol version
	version uint32
	// 1+	hash count	var_int	number of block locator hash entries
	// 32+	block locator hashes	char[32]	block locator object; newest back to genesis block (dense to start, but then sparse)
	blockLocatorHashes []Hash256

	// 32	hash_stop	char[32]	hash of the last desired block; set to zero to get as many blocks as possible (500)
	hashStop Hash256
}

func NewGetBlocks(blockLocatorHashes []string) *Getblocks {
	invs := make([]Hash256, len(blockLocatorHashes))
	for idx, id := range blockLocatorHashes {
		invs[idx] = NewHash256(id)
	}
	return &Getblocks{
		blockLocatorHashes: invs,
	}
}

// Visit
func (f *Getblocks) Visit(v MessageVisitor) {
	v.GetBlocks(f)
}

// Inv wire format
func (f *Getblocks) encode(coder BitcoinBincoder) {
	count := uint64(len(f.blockLocatorHashes))
	coder.VarInt(&count)
	coder.Slice(int(count), func(c int) {
		f.blockLocatorHashes = make([]Hash256, c)
	}, func(idx int) {
		coder.Hash256(&f.blockLocatorHashes[idx])
	})
}

// GetBlocks unmarshalling
func (coder *BitcoinReader) GetBlocks(f *Getblocks) {
	f.encode(coder)
}

// GetBlocks marshalling
func (coder *BitcoinWriter) GetBlocks(f *Getblocks) {
	f.encode(coder)
}

// Command code
func (f *Getblocks) Command() string {
	return GetblocksCmd
}

// Visual representation
func (f *Getblocks) Visual(w io.Writer) {
	fmt.Fprintf(w, "%s:\n", f.Command())
	for _, elm := range f.blockLocatorHashes {
		w.Write([]byte("- "))
		elm.Visual(w)
		w.Write([]byte("\n"))
	}
}
