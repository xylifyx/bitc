package protocol

import (
	"fmt"
	"io"
)

// GetData retrieves objects
type GetData struct {
	inventory []Inventory
}

func NewGetdata(objectType ObjectType, ids []string) *GetData {
	invs := make([]Inventory, len(ids))
	for idx, id := range ids {
		invs[idx] = NewInventory(objectType, id)
	}
	return &GetData{
		inventory: invs,
	}
}

// Command code
func (f *GetData) Command() string {
	return GetdataCmd
}

// Visit for Verack
func (f *GetData) Visit(v MessageVisitor) {
	v.Getdata(f)
}

// Inv wire format
func (f *GetData) encode(coder BitcoinBincoder, count *uint64) {
	coder.VarInt(count)
	coder.Slice(int(*count), func(c int) {
		f.inventory = make([]Inventory, c)
	}, func(idx int) {
		coder.Inventory(&f.inventory[idx])
	})
}

// Getdata unmarshalling
func (coder *BitcoinReader) Getdata(f *GetData) {
	var count uint64
	f.encode(coder, &count)
}

// Getdata marshalling
func (coder *BitcoinWriter) Getdata(f *GetData) {
	var count = uint64(len(f.inventory))
	f.encode(coder, &count)
}

// Visual representation
func (f *GetData) Visual(w io.Writer) {
	fmt.Fprintf(w, "%s:\n", f.Command())
	for _, elm := range f.inventory {
		w.Write([]byte("- "))
		elm.Visual(w)
		w.Write([]byte("\n"))
	}
}
