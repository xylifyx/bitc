package protocol

import (
	"fmt"
	"io"
)

// Block The block message is sent in response to a getdata message which requests transaction information from a block hash.
type Block struct {
	//	Field Size	Description	Data type	Comments
	//	4	version	int32_t	Block version information (note, this is signed)
	version int32
	//	32	prev_block	char[32]	The hash value of the previous block this particular block references
	prev_block Hash256
	//	32	merkle_root	char[32]	The reference to a Merkle tree collection which is a hash of all transactions related to this block
	merkle_root Hash256
	//	4	timestamp	uint32_t	A Unix timestamp recording when this block was created (Currently limited to dates before the year 2106!)
	timestamp uint32
	//	4	bits	uint32_t	The calculated difficulty target being used for this block
	bits uint32
	//	4	nonce	uint32_t	The nonce used to generate this block… to allow variations of the header and compute different hashes
	nonce uint32
	//	 ?	txn_count	var_int	Number of transaction entries
	//	 ?	txns	tx[]	Block transactions, in format of "tx" command
	txns []Tx
}

// Command code
func (f *Block) Command() string {
	return BlockCmd
}

// Visit for Verack
func (f *Block) Visit(v MessageVisitor) {
	v.Block(f)
}

// Inv wire format
func (f *Block) encode(coder BitcoinBincoder) {
	//4	version	int32_t	Block version information (note, this is signed)
	coder.I32(&f.version)
	//32	prev_block	char[32]	The hash value of the previous block this particular block references
	coder.Hash256(&f.prev_block)
	//32	merkle_root	char[32]	The reference to a Merkle tree collection which is a hash of all transactions related to this block
	coder.Hash256(&f.merkle_root)
	//4	timestamp	uint32_t	A Unix timestamp recording when this block was created (Currently limited to dates before the year 2106!)
	coder.UI32(&f.timestamp)
	//4	bits	uint32_t	The calculated difficulty target being used for this block
	coder.UI32(&f.bits)
	//4	nonce	uint32_t	The nonce used to generate this block… to allow variations of the header and compute different hashes
	coder.UI32(&f.nonce)
	//?	txn_count	var_int	Number of transaction entries
	//?	txns	tx[]	Block transactions, in format of "tx" command
	count := uint64(len(f.txns))
	coder.VarInt(&count)
	coder.Slice(int(count), func(c int) {
		f.txns = make([]Tx, c)
	}, func(idx int) {
		coder.Tx(&f.txns[idx])
	})
}

// GetBlocks unmarshalling
func (coder *BitcoinReader) Block(f *Block) {
	f.encode(coder)
}

// GetBlocks marshalling
func (coder *BitcoinWriter) Block(f *Block) {
	f.encode(coder)
}

// Visual representation
func (f *Block) Visual(w io.Writer) {
	fmt.Fprintf(w, "%s:\n", f.Command())
	for _, elm := range f.txns {
		w.Write([]byte("- "))
		elm.Visual(w)
	}
}
