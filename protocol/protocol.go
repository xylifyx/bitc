package protocol

import (
	"bufio"
	"errors"
	"fmt"
	"net"
	"os"
	"time"
)

// BitcoinPeer represents a connection to a peer
type BitcoinPeer struct {
	Version     *Version
	recvAddr    NetAddr
	conn        net.Conn
	outputCoder *BitcoinWriter
	inputCoder  *BitcoinReader
	magic       uint32
}

// Close the peer connection
func (peer *BitcoinPeer) Close() {
	fmt.Println("Close peer connection")
	peer.conn.Close()
}

func connectionString(ip net.IP, port uint16) string {
	return fmt.Sprintf("%v:%d", ip.String(), port)
}

// NewBitcoinPeer creates a peer connection
func NewBitcoinPeer(ip net.IP, port uint16, magic uint32) (*BitcoinPeer, error) {
	peer := BitcoinPeer{
		magic: magic,
	}
	peer.recvAddr = NetAddr{
		ip:       ip,
		port:     port,
		services: NodeNetwork,
	}
	connStr := connectionString(ip, port)
	conn, err := net.Dial("tcp", connStr)
	if err != nil {
		return nil, err
	}
	peer.conn = conn
	peer.inputCoder = NewBitcoinReader(bufio.NewReader(peer.conn))
	peer.outputCoder = NewBitcoinWriter(bufio.NewWriter(peer.conn))
	return &peer, nil
}

// VersionHandshake Send Version, Receive Version, (Send Verack | Receive Verack)
func (peer *BitcoinPeer) VersionHandshake() error {

	// Send version
	err := peer.SendMessage(&Version{
		version:   70015,
		userAgent: UserAgent,
		services:  NodeNetwork,
		timestamp: uint64(time.Now().Unix()),
		addrRecv:  peer.recvAddr,
		relay:     false,
	})
	if err != nil {
		return err
	}

	// Recieve Peer version
	var peerVersion Message
	err = peer.RecvMessage(&peerVersion)
	if err != nil {
		return err
	}
	switch peerVersion.(type) {
	case *Version:
		peer.Version = peerVersion.(*Version)
		break
	default:
		return errors.New("Expected Version message")
	}

	// Send Verack
	err = peer.SendMessage(&Verack{})
	if err != nil {
		return nil
	}

	// Receive Verack
	var peerVerack Message
	err = peer.RecvMessage(&peerVerack)
	if err != nil {
		return err
	}
	switch peerVerack.(type) {
	case *Verack:
		break
	default:
		return errors.New("Expected Verack message")
	}
	return nil
}

// SendMessage to a peer
func (peer *BitcoinPeer) SendMessage(payload Message) error {
	msg := MessageEnvelope{
		magic:   peer.magic,
		message: payload,
	}
	fmt.Printf("\tsend: %s\n", msg.message.Command())
	return peer.sendRequest(&msg)
}

// RecvMessage from a peer
func (peer *BitcoinPeer) RecvMessage(payload *Message) error {
	var msg MessageEnvelope
	err := peer.recvRequest(&msg)
	if err != nil {
		return err
	}
	*payload = msg.message
	fmt.Printf("\treceive: %s\n", msg.message.Command())
	return nil
}

func (peer *BitcoinPeer) sendRequest(msg *MessageEnvelope) error {
	peer.outputCoder.Message(msg)
	peer.outputCoder.Flush()
	return peer.outputCoder.Error()
}

func (peer *BitcoinPeer) recvRequest(msg *MessageEnvelope) error {
	peer.inputCoder.Message(msg)
	return peer.inputCoder.Error()
}

func (peer *BitcoinPeer) _receive() (Message, error) {
	var response Message
	err := peer.RecvMessage(&response)
	if err != nil {
		return nil, err
	}
	return response, nil
}

// Mempool return unconfirmed transactions
func (peer *BitcoinPeer) Mempool() error {
	return peer.SendMessage(&Mempool{})
}

// Mempool return address of other nodes
func (peer *BitcoinPeer) Getaddr() error {
	return peer.SendMessage(&Getaddr{})
}

func (peer *BitcoinPeer) Loop(visitor MessageVisitor) error {
	for {
		msg, err := peer._receive()
		if err != nil {
			fmt.Printf("error: %+v\n", err)
			return err
		}
		// d, err := yaml.Marshal(&msg)
		msg.Visit(visitor)
	}
}

func (peer *BitcoinPeer) Pong() error {
	return peer.SendMessage(&Pong{})
}
func (peer *BitcoinPeer) Getdata(objectType ObjectType, args []string) error {
	packet := NewGetdata(objectType, args)
	packet.Visual(os.Stdout)
	return peer.SendMessage(packet)
}

func (peer *BitcoinPeer) Getheaders(args []string) error {
	packet := NewGetheaders(args)
	packet.Visual(os.Stdout)
	return peer.SendMessage(packet)
}
