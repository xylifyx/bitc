package protocol

// Version as defined in the bitcoin protocol
type Version struct {
	// 4	version	int32_t	Identifies protocol version being used by the node
	version int32
	// 8	services	uint64_t	bitfield of features to be enabled for this connection
	services uint64
	// 8	timestamp	int64_t	standard UNIX timestamp in seconds
	timestamp uint64
	// 26	addr_recv	net_addr	The network address of the node receiving this message
	addrRecv NetAddr
	// Fields below require version ≥ 106
	// 26	addr_from	net_addr	The network address of the node emitting this message
	addrFrom NetAddr
	// 8	nonce	uint64_t	Node random nonce, randomly generated every time a version
	// packet is sent. This nonce is used to detect connections to self.
	nonce uint64
	// ?	user_agent	var_str	User Agent (0x00 if string is 0 bytes long)
	userAgent string
	// 4	start_height	int32_t	The last block received by the emitting node
	startHeight uint32
	// Fields below require version ≥ 70001
	// 1	relay	bool	Whether the remote peer should announce relayed transactions or not, see BIP 0037
	relay bool
}

func (f *Version) encode(w BitcoinBincoder) {
	// 4	version	int32_t	Identifies protocol version being used by the node
	w.I32(&f.version)
	// 8	services	uint64_t	bitfield of features to be enabled for this connection
	w.UI64(&f.services)
	// 8	timestamp	int64_t	standard UNIX timestamp in seconds
	w.UI64(&f.timestamp)
	// 26	addr_recv	net_addr	The network address of the node receiving this message
	w.NetAddr(&f.addrRecv, false)
	// Fields below require version ≥ 106
	// version is already initialized
	if f.version >= 107 {
		// 26	addr_from	net_addr	The network address of the node emitting this message
		w.NetAddr(&f.addrFrom, false)
		// 8	nonce	uint64_t	Node random nonce, randomly generated every time a version
		w.UI64(&f.nonce)
		// packet is sent. This nonce is used to detect connections to self.
		// ?	user_agent	var_str	User Agent (0x00 if string is 0 bytes long)
		w.VarStr(&f.userAgent)
		// 4	start_height	int32_t	The last block received by the emitting node
		w.UI32(&f.startHeight)
		if f.version >= 70001 {
			// Fields below require version ≥ 70001
			// 1	relay	bool	Whether the remote peer should announce relayed transactions or not, see BIP 0037
			w.Bool1(&f.relay)
		}
	}
}

// Version unmarhsall
func (c *BitcoinReader) Version(f *Version) {
	f.encode(c)
}

// Version marshall
func (c *BitcoinWriter) Version(f *Version) {
	f.encode(c)
}

// Visit for Version
func (f *Version) Visit(v MessageVisitor) {
	v.Version(f)
}

// Command version
func (f *Version) Command() string {
	return VersionCmd
}
