package protocol

type ObjectType uint32

/*
0	ERROR	Any data of with this number may be ignored
1	MSG_TX	Hash is related to a transaction
2	MSG_BLOCK	Hash is related to a data block
3	MSG_FILTERED_BLOCK	Hash of a block header; identical to MSG_BLOCK. Only to be used in getdata message. Indicates the reply should be a merkleblock message rather than a block message; this only works if a bloom filter has been set.
4	MSG_CMPCT_BLOCK	Hash of a block header; identical to MSG_BLOCK. Only to be used in getdata message. Indicates the reply should be a cmpctblock message. See BIP 152 for more info.
*/

const (
	ERROR              ObjectType = 0
	MSG_TX             ObjectType = 1
	MSG_BLOCK          ObjectType = 2
	MSG_FILTERED_BLOCK ObjectType = 3
	MSG_CMPCT_BLOCK    ObjectType = 4
)

const ProtocolVersion = 70015