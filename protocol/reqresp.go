// _Channels_ are the pipes that connect concurrent
// goroutines. You can send values into channels from one
// goroutine and receive those values into another
// goroutine.

package protocol

import (
	"os"
)

type ReqResp struct {
	Peer *BitcoinPeer
}

func (rr *ReqResp) Version(p *Version) {}
func (rr *ReqResp) Verack(p *Verack) {

}
func (rr *ReqResp) Mempool(p *Mempool) {

}
func (rr *ReqResp) UnsupportedMessage(p *UnsupportedMessage) {}
func (rr *ReqResp) Inv(p *Inv) {
	p.Visual(os.Stdout)
}
func (rr *ReqResp) Getdata(p *GetData) {
	p.Visual(os.Stdout)
}
func (rr *ReqResp) GetBlocks(p *Getblocks) {
	p.Visual(os.Stdout)
}
func (rr *ReqResp) Headers(p *Headers) {
	p.Visual(os.Stdout)
}
func (rr *ReqResp) Ping(p *Ping) {
	rr.Peer.Pong()
}
func (rr *ReqResp) Addr(p *Addr) {
	p.Visual(os.Stdout)
}
func (rr *ReqResp) Pong(p *Pong) {}
func (rr *ReqResp) Tx(p *Tx) {
	p.Visual(os.Stdout)
}
func (rr *ReqResp) Feefilter(p *Feefilter) {
	p.Visual(os.Stdout)
}
func (rr *ReqResp) Block(p *Block) {
	p.Visual(os.Stdout)
}

func (rr *ReqResp) Getheaders(p *Getheaders) {
	p.Visual(os.Stdout)
}

func (rr *ReqResp) Sendheaders(p *Sendheaders) {
}
func (rr *ReqResp) Getaddr(p *Getaddr) {
}
