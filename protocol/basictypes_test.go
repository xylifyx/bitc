package protocol

import (
	"reflect"
	"testing"
)

func Test_varStr_Marshall(t *testing.T) {
	expected := "hello"

	coded, err := Marshall(func(w *BitcoinWriter) {
		w.VarStr(&expected)
	})

	if err != nil {
		t.Errorf("Marshall error: %v", err)
	}

	var got string

	err = Unmarshall(func(r *BitcoinReader) {
		r.VarStr(&got)
	}, coded)

	if err != nil {
		t.Errorf("Unmarshall error: %v", err)
	}

	if !reflect.DeepEqual(got, expected) {
		t.Errorf("varStr bincoding: expected %v got %v", expected, got)
	}

}
