package protocol

import (
	_ "crypto/elliptic"
	"math/big"
	"bytes"
	"strings"
)

const (
	// Decimal version	Leading symbol	Use
	MainnetPubkey byte = 0 // 1	Bitcoin pubkey hash
	MainnetScript byte = 5 // 3	Bitcoin script hash
	//21	// 4	Bitcoin (compact) public key (proposed)
	//52	// M or N	Namecoin pubkey hash
	MainnetPrivateKey byte = 128 // 5	Private key
	TestnetPubkey     byte = 111 // m or n	Bitcoin testnet pubkey hash
	TestnetScript     byte = 196 // 2	Bitcoin testnet script hash
	TestnetPrivateKey byte = 239 // 9	92Pg46rUhgTT7romnV7iGW6W1gbGdeezqdbJCzShkCsYNzyyNcc or cNJFgo1driFnPcBdBX8BrJrpxchBWXwXCvNH5SoSkdcF6JXXwHMm
)
const code_string = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz"

func Base58Enc(version byte, key []byte) string {
	k := make([]byte, len(key)+5)
	k[0] = byte(version)
	copy(k[1:(len(key) + 1)], key)
	sum := dhash(k[0:len(key)+1])
	copy(k[len(key)+1:], sum[:])
	return string(_Base58(k))
}

func _Base58(key []byte) []byte {
	x := new(big.Int).SetBytes(key)
	var output_string bytes.Buffer
	base58encInt(x, &output_string)
	s := output_string.Bytes()
	reverse(s)
	return s
}

func base58encInt(x *big.Int, output_string *bytes.Buffer) {
	c58 := big.NewInt(58)
	var q, r big.Int
	for x.Sign() == 1 {
		q.DivMod(x, c58, &r)
		output_string.WriteByte(code_string[r.Int64()])
		x.Set(&q)
	}
}

func Base58Dec(key string) ([]byte,byte) {
	b := []byte(key)
	// reverse(b)

	c58 := big.NewInt(58)
	n := big.NewInt(0)
	x := big.NewInt(0)
	for _, e := range b {
		n.Mul(n, c58)
		x.SetInt64(int64(strings.IndexByte(code_string, e)))
		n.Add(n, x)
	}
	b = n.Bytes()
	payload := b[1:len(b)-4]
	version := b[0]
	hash := b[len(b)-4:len(b)]
	chash := dhash(b[0:len(b)-4])[0:4]
	if !bytes.Equal(hash ,chash) {
		return nil,255
	}
	return payload,version
}
