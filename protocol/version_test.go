package protocol

import (
	"reflect"
	"testing"
)

func Test_version_message_bincode(t *testing.T) {
	msg := MessageEnvelope{
		magic: MainnetMagic,
		message: &Version{
			version:   31900,
			timestamp: 0x4D1015E6,
			addrFrom: NetAddr{ip: IPOf("10.0.0.2"), port: 8333,
				services: NodeNetwork},
			addrRecv: NetAddr{ip: IPOf("10.0.0.1"), port: 8333,
				services: NodeNetwork},
			nonce:       0x1357B43A2C209DDD,
			userAgent:   "",
			startHeight: 0x018155,
			services:    NodeNetwork,
		},
	}
	wanthex := `
- Message header
 F9 BE B4 D9                                                                   - Main network magic bytes
 76 65 72 73 69 6F 6E 00 00 00 00 00                                           - "version" command
 55 00 00 00                                                                   - Payload is 85 bytes long
 B7 A8 27 97                                                                   - No checksum in version message until 20 February 2012. See https://bitcointalk.org/index.php?topic=55852.0
- Version message:
 9C 7C 00 00                                                                   - 31900 (version 0.3.19)
 01 00 00 00 00 00 00 00                                                       - 1 (NODE_NETWORK services)
 E6 15 10 4D 00 00 00 00                                                       - Mon Dec 20 21:50:14 EST 2010
 01 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 FF FF 0A 00 00 01 20 8D - Recipient address info - see Network Address
 01 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 FF FF 0A 00 00 02 20 8D - Sender address info - see Network Address
 DD 9D 20 2C 3A B4 57 13                                                       - Node random unique ID
 00                                                                            - "" sub-version string (string is 0 bytes long)
 55 81 01 00                                                                   - Last block sending node has is block #98645"
`
	want := dehex(wanthex)

	got, _ := Marshall(func(bw *BitcoinWriter) {
		bw.Message(&msg)
	})

	if len(got) != len(want) {
		t.Errorf("%q. %v not expected, want %v", "version", len(got), len(want))
	}

	if !reflect.DeepEqual(got, want) {
		t.Errorf("%q. version.Marshall() = \n%v, want \n%v", "version", got, want)
	}

}

func Test_version_Marshall(t *testing.T) {
	v := Version{
		version:   31900,
		timestamp: 0x4D1015E6,
		addrFrom: NetAddr{ip: IPOf("10.0.0.2"), port: 8333,
			services: NodeNetwork},
		addrRecv: NetAddr{ip: IPOf("10.0.0.1"), port: 8333,
			services: NodeNetwork},
		nonce:       0x1357B43A2C209DDD,
		userAgent:   "",
		startHeight: 0x018155,
		services:    NodeNetwork,
	}
	wanthex := `
- Version message:
 9C 7C 00 00                                                                   - 31900 (version 0.3.19)
 01 00 00 00 00 00 00 00                                                       - 1 (NODE_NETWORK services)
 E6 15 10 4D 00 00 00 00                                                       - Mon Dec 20 21:50:14 EST 2010
 01 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 FF FF 0A 00 00 01 20 8D - Recipient address info - see Network Address
 01 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 FF FF 0A 00 00 02 20 8D - Sender address info - see Network Address
 DD 9D 20 2C 3A B4 57 13                                                       - Node random unique ID
 00                                                                            - "" sub-version string (string is 0 bytes long)
 55 81 01 00                                                                   - Last block sending node has is block #98645"
`
	want := dehex(wanthex)

	got, err := Marshall(func(bw *BitcoinWriter) {
		bw.Version(&v)
	})

	if err != nil {
		t.Errorf("%q. version.Marshall() error %v", "version", err)
	}

	if len(got) != len(want) {
		t.Errorf("%q. %v not expected, want %v", "version", len(got), len(want))
	}

	if !reflect.DeepEqual(got, want) {
		t.Errorf("%q. version.Marshall() = \n%v, want \n%v", "version", got, want)
	}

}

func Test_unmarshall(t *testing.T) {
	hex := `
- Message Header:
 F9 BE B4 D9                                                                   - Main network magic bytes
 76 65 72 73 69 6F 6E 00 00 00 00 00                                           - "version" command
 64 00 00 00                                                                   - Payload is 100 bytes long
 3B 64 8D 5A                                                                   - payload checksum

- Version message:
 62 EA 00 00                                                                   - 60002 (protocol version 60002)
 01 00 00 00 00 00 00 00                                                       - 1 (NODE_NETWORK services)
 11 B2 D0 50 00 00 00 00                                                       - Tue Dec 18 10:12:33 PST 2012
 01 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 FF FF 00 00 00 00 00 00 - Recipient address info - see Network Address
 01 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 FF FF 00 00 00 00 00 00 - Sender address info - see Network Address
 3B 2E B3 5D 8C E6 17 65                                                       - Node ID
 0F 2F 53 61 74 6F 73 68 69 3A 30 2E 37 2E 32 2F                               - "/Satoshi:0.7.2/" sub-version string (string is 15 bytes long)
 C0 3E 03 00                                                                   - Last block sending node has is block #212672
 `
	want := MessageEnvelope{
		magic:    MainnetMagic,
		command:  VersionCmd,
		length:   100,
		checksum: [4]byte{0x3B, 0x64, 0x8D, 0x5A},
		message: &Version{
			version:     60002,
			timestamp:   0x50D0B211,
			addrFrom:    NetAddr{ip: IPOf("0.0.0.0"), services: NodeNetwork},
			addrRecv:    NetAddr{ip: IPOf("0.0.0.0"), services: NodeNetwork},
			nonce:       0x6517e68c5db32e3b,
			userAgent:   "/Satoshi:0.7.2/",
			startHeight: 212672,
			services:    NodeNetwork,
		},
	}

	var got MessageEnvelope
	err := Unmarshall(func(br *BitcoinReader) {
		br.Message(&got)
	}, dehex(hex))

	if err != nil {
		t.Errorf("%q. version.Marshall() error %v", "version", err)
	}

	if !reflect.DeepEqual(got, want) {
		t.Errorf("%q. version.Marshall() = \n%v, want \n%v", "version", got, want)
	}
}
