package protocol

import (
	"encoding/hex"
	"fmt"
	"io"
	"bytes"
)

/*
4	version	int32_t	Transaction data format version (note, this is signed)
1+	tx_in count	var_int	Number of Transaction inputs
41+	tx_in	tx_in[]	A list of 1 or more transaction inputs or sources for coins
1+	tx_out count	var_int	Number of Transaction outputs
9+	tx_out	tx_out[]	A list of 1 or more transaction outputs or destinations for coins
4	lock_time	uint32_t	The block number or timestamp at which this transaction is unlocked:
Value	Description
0	Not locked
< 500000000	Block number at which this transaction is unlocked
>= 500000000	UNIX timestamp at which this transaction is unlocked
If all TxIn inputs have final (0xffffffff) sequence numbers then lock_time is irrelevant. Otherwise, the transaction may not be added to a block until after lock_time (see NLockTime).
*/
// Tx is the response with transactions
type Tx struct {
	/*
		4	version	int32_t	Transaction data format version (note, this is signed)
		1+	tx_in count	var_int	Number of Transaction inputs
		41+	tx_in	tx_in[]	A list of 1 or more transaction inputs or sources for coins
		1+	tx_out count	var_int	Number of Transaction outputs
		9+	tx_out	tx_out[]	A list of 1 or more transaction outputs or destinations for coins
		4	lock_time	uint32_t	The block number or timestamp at which this transaction is unlocked:
	*/
	id       Hash256
	Version  int32
	TxIn     []TxIn
	TxOut    []TxOut
	LockTime uint32
}

// Tx wire format
func (f *Tx) encode(coder BitcoinBincoder) {
	coder.I32(&f.Version)
	{
		count := uint64(len(f.TxIn))
		coder.VarInt(&count)
		coder.Slice(int(count), func(size int) {
			f.TxIn = make([]TxIn, count)
		}, func(idx int) {
			coder.TxIn(&f.TxIn[idx])
		})
	}
	{
		count := uint64(len(f.TxOut))
		coder.VarInt(&count)
		coder.Slice(int(count), func(size int) {
			f.TxOut = make([]TxOut, count)
		}, func(idx int) {
			coder.TxOut(&f.TxOut[idx])
		})
	}
	coder.UI32(&f.LockTime)
}

type TxIn struct {
	PreviousOutput  OutPoint
	SignatureScript []byte
	Sequence        uint32
	// 36	previous_output	outpoint	The previous output transaction reference, as an OutPoint structure
	// 1+	script length	var_int	The length of the signature script
	// ?	signature script	uchar[]	Computational Script for confirming transaction authorization
	// 4	sequence	uint32_t	Transaction version as defined by the sender. Intended for "replacement" of transactions when information is updated before inclusion into a block.
}

// Tx wire format
func (f *TxIn) encode(coder BitcoinBincoder) {
	coder.OutPoint(&f.PreviousOutput)
	scriptLength := uint64(len(f.SignatureScript))
	coder.VarInt(&scriptLength)
	coder.ByteSlice(&f.SignatureScript, int(scriptLength))
	coder.UI32(&f.Sequence)
}

type OutPoint struct {
	// 32	hash	char[32]	The hash of the referenced transaction.
	// 4	index	uint32_t	The index of the specific output in the transaction. The first output is 0, etc.
	Hash  Hash256
	Index uint32
}

// Tx wire format
func (f *OutPoint) encode(coder BitcoinBincoder) {
	coder.Hash256(&f.Hash)
	coder.UI32(&f.Index)
}

type TxOut struct {
	// 8	value	int64_t	Transaction Value
	// 1+	pk_script length	var_int	Length of the pk_script
	// ?	pk_script	uchar[]	Usually contains the public key as a Bitcoin script setting up conditions to claim this output.
	Value    int64
	PkScript []byte
}

// Tx wire format
func (f *TxOut) encode(coder BitcoinBincoder) {
	coder.I64(&f.Value)
	scriptLength := uint64(len(f.PkScript))
	coder.VarInt(&scriptLength)
	coder.ByteSlice(&f.PkScript, int(scriptLength))
}

// Command code
func (f *Tx) Command() string {
	return TxCmd
}

// Visit for Verack
func (f *Tx) Visit(v MessageVisitor) {
	v.Tx(f)
}

// Visual representation
func (f *Tx) Visual(w io.Writer) {
	fmt.Fprintf(w, "%s: id: %s version: %d\n", f.Command(), f.id.GetId(), f.Version)
	fmt.Fprintf(w, "    txin:\n")
	for _, elm := range f.TxIn {
		w.Write([]byte("    - "))
		elm.Visual(w)
		w.Write([]byte("\n"))
	}
	fmt.Fprintf(w, "    txout:\n")
	for _, elm := range f.TxOut {
		w.Write([]byte("    - "))
		elm.Visual(w)
		w.Write([]byte("\n"))
	}
}

func (f *TxIn) Visual(w io.Writer) {
	fmt.Fprintf(w, "%s[%d]", f.PreviousOutput.Hash.GetId(), f.PreviousOutput.Index)
}

func (f *TxOut) Visual(w io.Writer) {
	fmt.Fprintf(w, "%d %s", f.Value, hex.EncodeToString(f.PkScript))
}

// Boilerplate


// Tx unmarshalling
func (coder *BitcoinReader) Tx(f *Tx) {
	var buf bytes.Buffer
	bcoder := coder.tee(&buf)
	f.encode(&bcoder)
	f.id = dhash256(buf.Bytes())
}

// Tx marshalling
func (coder *BitcoinWriter) Tx(f *Tx) {
	f.encode(coder)
}

// Tx marshalling
func (coder *BitcoinWriter) TxIn(f *TxIn) {
	f.encode(coder)
}

// Tx unmarshalling
func (coder *BitcoinReader) TxIn(f *TxIn) {
	f.encode(coder)
}

// Tx marshalling
func (coder *BitcoinWriter) TxOut(f *TxOut) {
	f.encode(coder)
}

// Tx unmarshalling
func (coder *BitcoinReader) TxOut(f *TxOut) {
	f.encode(coder)
}

// Tx unmarshalling
func (coder *BitcoinReader) OutPoint(f *OutPoint) {
	f.encode(coder)
}

// Tx marshalling
func (coder *BitcoinWriter) OutPoint(f *OutPoint) {
	f.encode(coder)
}
