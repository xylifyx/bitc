package protocol

// BlockTransactionsRequest structure is used to list transaction indexes in a block being requested.
type BlockTransactionsRequest struct {
	// Binary blob	32 bytes	The output from a double-SHA256 of the block header, as used elsewhere	The blockhash of the block which the transactions being requested are in
	blockhash [32]byte
	// CompactSize 1 or 3 bytes	As used to encode array lengths elsewhere	The number of transactions being requested
	indexesLength int
	// List of CompactSizes	1 or 3 bytes*indexes_length	Differentially
	//encoded	The indexes of the transactions being requested in the block
	//See BIP 152 for more information.
	indexes []int
}

// BlockTransactions structure is used to provide some of the transactions in a block, as requested.
type BlockTransactions struct {
	// Binary blob	32 bytes	The output from a double-SHA256 of the block header, as used elsewhere	The blockhash of the block which the transactions being provided are in
	blockhash          []byte
	transactionsLength int   // CompactSize	1 or 3 bytes	As used to encode array lengths elsewhere	The number of transactions provided
	transactions       []int //List of Transactions	variable	As encoded in tx messages	The transactions provided
}
