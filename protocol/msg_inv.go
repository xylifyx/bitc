package protocol

import (
	"encoding/hex"
	"fmt"
	"io"
)

// Inv is the response with transactions
type Inv struct {
	inventory []Inventory
}

// 1	MSG_TX	The hash is a TXID.
// 2	MSG_BLOCK	The hash is of a block header.
// 3	MSG_FILTERED_BLOCK	The hash is of a block header;
//    identical to MSG_BLOCK. When used in a getdata message,
//   this indicates the response should be a merkleblock message rather

// Inventory Entry
type Inventory struct {
	Type uint32
	Hash Hash256
}

// Command code
func (f *Inv) Command() string {
	return InvCmd
}

// Visit for Verack
func (f *Inv) Visit(v MessageVisitor) {
	v.Inv(f)
}

// Inv wire format
func (f *Inv) encode(coder BitcoinBincoder, count *uint64) {
	coder.VarInt(count)
	coder.Slice(int(*count), func(c int) {
		f.inventory = make([]Inventory, c)
	}, func(idx int) {
		coder.Inventory(&f.inventory[idx])
	})
}

// Inv unmarshalling
func (coder *BitcoinReader) Inv(f *Inv) {
	var count uint64
	f.encode(coder, &count)
}

// Inv marshalling
func (coder *BitcoinWriter) Inv(f *Inv) {
	var count = uint64(len(f.inventory))
	f.encode(coder, &count)
}

// Inv unmarshalling
func (coder *BitcoinReader) Inventory(f *Inventory) {
	f.encode(coder)
}

// Inv marshalling
func (coder *BitcoinWriter) Inventory(f *Inventory) {
	f.encode(coder)
}

// Inventory wire format
func (f *Inventory) encode(coder BitcoinBincoder) {
	coder.UI32(&f.Type)
	coder.Hash256(&f.Hash)
}

func NewInventory(objectType ObjectType, id string) Inventory {
	return Inventory{
		Type: uint32(objectType),
		Hash: NewHash256(id),
	}
}

func NewInv(objectType ObjectType, ids []string) *Inv {
	invs := make([]Inventory, len(ids))
	for idx, id := range ids {
		invs[idx] = NewInventory(objectType, id)
	}
	return &Inv{
		inventory: invs,
	}
}

func (f *Hash256) GetId() string {
	s := *f
	for i, j := 0, len(s)-1; i < j; i, j = i+1, j-1 {
		s[i], s[j] = s[j], s[i]
	}
	return hex.EncodeToString(s[:])
}

func NewHash256(id string) Hash256 {
	s, _ := hex.DecodeString(id)
	for i, j := 0, len(s)-1; i < j; i, j = i+1, j-1 {
		s[i], s[j] = s[j], s[i]
	}
	var hash Hash256
	copy(hash[:], s)
	return hash
}

func (f *Inventory) Visual(w io.Writer) {
	fmt.Fprintf(w, "%d %s", f.Type, f.Hash.GetId())
}

// Visual representation
func (f *Inv) Visual(w io.Writer) {
	fmt.Fprintf(w, "%s: #%d\n", f.Command(), len(f.inventory))
	for idx, elm := range f.inventory {
		w.Write([]byte("- "))
		elm.Visual(w)
		w.Write([]byte("\n"))
		if idx > 5 {
			w.Write([]byte("...\n"))
			break
		}
	}
}
