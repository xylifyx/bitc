package protocol

// Mempool is the reqeust for unconfirmed transactions
type Getaddr struct {
}

// Mempool wire format
func (f *Getaddr) encode(w BitcoinBincoder) {
}

// Mempool unmarshalling
func (coder *BitcoinReader) Getaddr(f *Getaddr) {
	f.encode(coder)
}

// Mempool marshalling
func (coder *BitcoinWriter) Getaddr(f *Getaddr) {
	f.encode(coder)
}

// Visit for Mempool
func (f *Getaddr) Visit(v MessageVisitor) {
	v.Getaddr(f)
}

// Command name of Mempool
func (f *Getaddr) Command() string {
	return GetaddrCmd
}
