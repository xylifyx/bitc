package protocol

import (
	"net"

	"github.com/xylifyx/bincoder"
	"bytes"
)

// BitcoinBincoder contains Bitcoin specific bincoders
type BitcoinBincoder interface {
	// Packet interface
	MessagePackage(command string, f *Message)
	// Message Package types
	MessageVisitor
	// Standard bincoder types
	bincoder.Bincoder

	// primitive types
	VarStr(f *string)
	NetAddr(f *NetAddr, timePresent bool)
	netIP(f *net.IP)
	Bool1(f *bool)
	UI16no(f *uint16)
	Hash256(f *Hash256)

	// complex sub types
	Inventory(f *Inventory)
	BlockHeader(*BlockHeader)
	TxIn(*TxIn)
	TxOut(*TxOut)
	OutPoint(*OutPoint)
}

// BitcoinReader extensions to the bincoder reader
type BitcoinReader struct {
	bincoder.BinReader
}

// BitcoinWriter extensions to the bincoder reader
type BitcoinWriter struct {
	bincoder.BinWriter
}

// Marshall to byte slice
func Marshall(m func(bw *BitcoinWriter)) ([]byte, error) {
	bw := BitcoinWriter{}
	result := bincoder.Marshall(func(w bincoder.Writer) {
		bw.SetWriter(w)
		m(&bw)
	})
	return result, bw.Error()
}

// Unmarshall from byte slice
func Unmarshall(um func(br *BitcoinReader), data []byte) error {
	br := BitcoinReader{}
	bincoder.Unmarshall(func(r bincoder.Reader) {
		br.SetReader(r)
		um(&br)
	}, data)
	return br.Error()
}

// Unmarshall from byte slice
func (r *BitcoinReader) tee(buf *bytes.Buffer) BitcoinReader {
	br := BitcoinReader{}
	br.SetReader(&TeeReader {
		reader: r,
		buf: buf,
	})
	return br
}

type TeeReader struct {
	buf *bytes.Buffer
	reader bincoder.Reader
}

func (r *TeeReader) Read(d []byte) (int,error) {
	n,err := r.reader.Read(d)
	if err != nil {
		return 0,err
	}
	r.buf.Write(d[0:n])
	return n,nil
}

func (r *TeeReader) dhash() []byte {
	return dhash(r.buf.Bytes())
}

// NewBitcoinWriter factory
func NewBitcoinWriter(writer bincoder.Writer) *BitcoinWriter {
	bw := BitcoinWriter{}
	bw.SetWriter(writer)
	return &bw
}

// NewBitcoinReader factory
func NewBitcoinReader(reader bincoder.Reader) *BitcoinReader {
	br := BitcoinReader{}
	br.SetReader(reader)
	return &br
}
