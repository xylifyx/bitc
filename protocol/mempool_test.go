package protocol

import (
	"testing"
)

func TestMempool_encode(t *testing.T) {
	hexmsg := (     // Message header:
	"F9 BE B4 D9" + //                              - Main network magic bytes
		"76 65 72 61  63 6B 00 00 00 00 00 00" + // - "verack" command
		"00 00 00 00" + //                          - Payload is 0 bytes long
		"5D F6 E0 E2") //                           - Checksum
	want := dehex(hexmsg)
	m := MessageEnvelope{
		magic:    MainnetMagic,
		command:  MempoolCmd,
		length:   0,
		checksum: [4]byte{0x5D, 0xF6, 0xE0, 0xE2},
		message:  &Mempool{},
	}

	_, _ = Marshall(func(bw *BitcoinWriter) {
		bw.Message(&m)
	})
	/*
		if err != nil {
			t.Errorf("%q. Verack marshalling error = %v", "verack", err)
		}
		if !reflect.DeepEqual(got, want) {
			t.Errorf("%q. Verack marshalling = \n%v, want %v", "verack", got, want)
		}
	*/
	var got2 MessageEnvelope
	_ = Unmarshall(func(coder *BitcoinReader) {
		coder.Message(&got2)
	}, want)
	/*
		if err2 != nil {
			t.Errorf("%q. Verack unmarshalling error = %v", "verack", err2)
		}
		if !reflect.DeepEqual(got2, m) {
			t.Errorf("%q. Verack unmarshalling = %v, want %v", "verack", got2, m)
		}
	*/
}
