package protocol

// Mempool is the reqeust for unconfirmed transactions
type UnsupportedMessage struct {
	command string
}

// Mempool unmarshalling
func (coder *BitcoinReader) UnsupportedMessage(f *UnsupportedMessage) {

}

// Mempool marshalling
func (coder *BitcoinWriter) UnsupportedMessage(f *UnsupportedMessage) {

}

// Command name of Verack
func (f *UnsupportedMessage) Command() string {
	return f.command
}

// Visit for Verack
func (f *UnsupportedMessage) Visit(v MessageVisitor) {
	v.UnsupportedMessage(f)
}
