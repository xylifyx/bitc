package protocol

// Mempool is the reqeust for unconfirmed transactions
type Mempool struct {
}

// Mempool wire format
func (f *Mempool) encode(w BitcoinBincoder) {
}

// Mempool unmarshalling
func (coder *BitcoinReader) Mempool(f *Mempool) {
	f.encode(coder)
}

// Mempool marshalling
func (coder *BitcoinWriter) Mempool(f *Mempool) {
	f.encode(coder)
}

// Visit for Mempool
func (f *Mempool) Visit(v MessageVisitor) {
	v.Mempool(f)
}

// Command name of Mempool
func (f *Mempool) Command() string {
	return MempoolCmd
}
