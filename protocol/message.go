package protocol

import (
	"bytes"
	"fmt"
)

// MessageEnvelope format
type MessageEnvelope struct {
	// 4	magic	uint32_t	Magic value indicating message origin network, and used to seek to next message when stream state is unknown
	magic uint32
	// 12	command	char[12]	ASCII string identifying the packet content, NULL padded (non-NULL padding results in packet rejected)
	command string
	// 4	length	uint32_t	Length of payload in number of bytes
	length uint32
	// 4	checksum	uint32_t	First 4 bytes of sha256(sha256(payload))
	checksum [4]byte
	// ?	message	uchar[]	The actual data
	message Message
}

// Known magic values:
const (
	// MainnetMagic	0xD9B4BEF9	F9 BE B4 D9
	MainnetMagic = 0xD9B4BEF9

	// TestnetMagic	0xDAB5BFFA	FA BF B5 DA
	TestnetMagic = 0xDAB5BFFA

	// Testnet3Magic	0x0709110B	0B 11 09 07
	Testnet3Magic = 0x0709110B

	// NamecoinMagic	0xFEB4BEF9	F9 BE B4 FE
	NamecoinMagic = 0xFEB4BEF9
)

const (
	// UserAgent sent in version message
	UserAgent = "/xylifyx.bitc/"
)

const (
	// NodeNetwork node can be asked for full blocks instead of just headers.
	NodeNetwork = 1 //
	// NodeGetUTXO See BIP 0064
	NodeGetUTXO = 2
	// NodeBloom See BIP 0111
	NodeBloom = 4 //See BIP 0111
)

// Message reader
func (coder *BitcoinReader) Message(f *MessageEnvelope) {
	var payload []byte
	f.bincode(coder, &payload)
	if coder.Error() != nil {
		return
	}
	err := Unmarshall(func(br *BitcoinReader) {
		br.MessagePackage(f.command, &f.message)
	}, payload)
	if err != nil {
		coder.SetError(err)
	}
}

// Message writer
func (coder *BitcoinWriter) Message(f *MessageEnvelope) {
	f.command = f.message.Command()
	payload, err := Marshall(func(coder *BitcoinWriter) {
		coder.MessagePackage(f.command, &f.message)
	})
	if err != nil {
		coder.SetError(err)
		return
	}
	// make sure the length and checksum is updated with the marshalled payload
	f.length = uint32(len(payload))
	copy(f.checksum[0:4], dhash(payload)[0:4])

	if f.magic == 0 {
		f.magic = MainnetMagic
	}

	f.bincode(coder, &payload)
}

func (f *MessageEnvelope) bincode(coder BitcoinBincoder, payload *[]byte) {
	coder.UI32(&f.magic)

	if f.magic == MainnetMagic || f.magic == Testnet3Magic {
	} else {
		coder.SetError(fmt.Errorf("Unknown magic %x", f.magic))
		return
	}
	coder.Bytes(12, func() []byte {
		return []byte(f.command)
	}, func(p []byte) {
		f.command = zeroTerminated(p)
	})
	coder.UI32(&f.length)
	coder.Bytes(4, func() []byte {
		return f.checksum[0:4]
	}, func(p []byte) {
		copy(f.checksum[0:4], p[0:4])
	})

	coder.ByteSlice(payload, int(f.length))
}

func zeroTerminated(s []byte) string {
	b := bytes.SplitN(s, []byte{0}, 2)[0]
	return string(b)
}

const (
	VersionCmd     = "version"
	VerackCmd      = "verack"
	MempoolCmd     = "mempool"
	InvCmd         = "inv"
	sendheadersCmd = "sendheaders"
	sendcmpctCmd   = "sendcmpct"
	AddrCmd        = "addr"
	getheadersCmd  = "getheaders"
	FeefilterCmd   = "feefilter"
	GetdataCmd     = "getdata"
	HeadersCmd     = "headers"
	PingCmd        = "ping"
	PongCmd        = "pong"
	TxCmd          = "tx"
	GetblocksCmd   = "getblocks"
	BlockCmd       = "block"
	GetaddrCmd     = "getaddr"
)

// Message all bitcoin packages must implement this
type Message interface {
	Command() string
	Visit(MessageVisitor)
}

// MessageVisitor used by Visit on MessagePackage
type MessageVisitor interface {
	Version(*Version)
	Verack(*Verack)
	Mempool(*Mempool)
	UnsupportedMessage(*UnsupportedMessage)
	Inv(*Inv)
	Getdata(*GetData)
	Headers(*Headers)
	Ping(*Ping)
	Pong(*Pong)
	Addr(*Addr)
	Tx(*Tx)
	Feefilter(*Feefilter)
	GetBlocks(*Getblocks)
	Block(*Block)
	Getheaders(*Getheaders)
	Sendheaders(*Sendheaders)
	Getaddr(*Getaddr)
}

// NewMessagePackage Creates a new message package based on the command name
func NewMessagePackage(command string) Message {
	switch command {
	case VersionCmd:
		return &Version{}
	case VerackCmd:
		return &Verack{}
	case MempoolCmd:
		return &Mempool{}
	case InvCmd:
		return &Inv{}
	case GetdataCmd:
		return &GetData{}
	case HeadersCmd:
		return &Headers{}
	case PingCmd:
		return &Ping{}
	case PongCmd:
		return &Pong{}
	case AddrCmd:
		return &Addr{}
	case FeefilterCmd:
		return &Feefilter{}
	case TxCmd:
		return &Tx{}
	case GetblocksCmd:
		return &Getblocks{}
	case BlockCmd:
		return &Block{}
	case getheadersCmd:
		return &Getheaders{}
	case sendheadersCmd:
		return &Sendheaders{}
	case GetaddrCmd:
		return &Getaddr{}
	default:
		return &UnsupportedMessage{command: command}
	}
}

// MessagePackage writer
func (coder *BitcoinWriter) MessagePackage(command string, f *Message) {
	(*f).Visit(coder)
}

// MessagePackage reader
func (coder *BitcoinReader) MessagePackage(command string, f *Message) {
	p := NewMessagePackage(command)
	*f = p
	p.Visit(coder)
}
